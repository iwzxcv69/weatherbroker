package ru.ivankov.weatherBroker.dao;

import org.springframework.data.repository.CrudRepository;
import ru.ivankov.weatherBroker.model.Weather;

public interface WeatherRepository extends CrudRepository<Weather, Long> {
}
