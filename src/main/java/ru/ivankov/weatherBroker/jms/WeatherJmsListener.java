package ru.ivankov.weatherBroker.jms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.ivankov.weatherBroker.dao.WeatherRepository;
import ru.ivankov.weatherBroker.model.Weather;


@Component
public class WeatherJmsListener {

    private WeatherRepository actualRepository;

    @Autowired
    public WeatherJmsListener(WeatherRepository actualRepository) {
        this.actualRepository = actualRepository;
    }

    @JmsListener(destination = "weathercondition")
    @Transactional
    public void receiveWeatherCondition(final Weather condition) {
        actualRepository.save(condition);
    }

}
