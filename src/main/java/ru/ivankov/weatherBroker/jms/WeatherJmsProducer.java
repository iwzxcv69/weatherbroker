package ru.ivankov.weatherBroker.jms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import ru.ivankov.weatherBroker.model.WeatherDto;

@Component
public class WeatherJmsProducer {

    private JmsTemplate jmsTemplate;

    @Autowired
    public WeatherJmsProducer(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    public void sendActualWeather(final String topicName, final WeatherDto condition) {
        jmsTemplate.convertAndSend(topicName, condition);
    }

}
