package ru.ivankov.weatherBroker.service;

public interface CityWeatherService {

    void getWeatherForCity(String cityName);
}
