package ru.ivankov.weatherBroker.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import ru.ivankov.weatherBroker.error.CityNotFoundException;
import ru.ivankov.weatherBroker.jms.WeatherJmsProducer;
import ru.ivankov.weatherBroker.model.Weather;
import ru.ivankov.weatherBroker.model.WeatherDto;
import ru.ivankov.weatherBroker.model.WeatherMapper;


import java.net.URI;


@Service
@Repository
@Transactional
public class WeatherService {

    private static final String URL_WEATHER = "http://api.openweathermap.org/data/2.5/weather?q=";
    private static final String URL_API_KEY = "&units=metric&appid=86ad4f31429340c12f92152348afbbaf";
    private static final String TOPIC_NAME_ACTUAL_WEATHER = "weather";

    private WeatherJmsProducer jmsProducer;
    private RestTemplate restTemplate;

    @Autowired
    public WeatherService(WeatherJmsProducer jmsProducer, RestTemplate restTemplate) {
        this.jmsProducer = jmsProducer;
        this.restTemplate = restTemplate;
    }

    public Weather getWeatherInTheCity(CityWeatherService city) {
        try {
            URI finalURL = new URI(URL_WEATHER + city + URL_API_KEY);
            WeatherDto response = restTemplate.getForObject(finalURL, WeatherDto.class);
            jmsProducer.sendActualWeather(TOPIC_NAME_ACTUAL_WEATHER, response);
            return WeatherMapper.fromDto(response);

        } catch (Exception e) {
            throw new CityNotFoundException();
        }
    }
}