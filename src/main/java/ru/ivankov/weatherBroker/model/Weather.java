package ru.ivankov.weatherBroker.model;



import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "weather")
public class Weather implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "city")
    private String city;

    @Column(name = "temperature_now")
    private double temperatureNow;

    @Column(name = "temperature_felt")
    private double temperatureFelt;

    @Column(name = "temperature_max")
    private double temperatureMax;

    @Column(name = "temperature_min")
    private double temperatureMin;

    @Column(name = "pressure")
    private double pressure;

    @Column(name = "humidity")
    private double humidity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public double getTemperatureNow() {
        return temperatureNow;
    }

    public void setTemperatureNow(double temperatureNow) {
        this.temperatureNow = temperatureNow;
    }

    public double getTemperatureFelt() {
        return temperatureFelt;
    }

    public void setTemperatureFelt(double temperatureFelt) {
        this.temperatureFelt = temperatureFelt;
    }

    public double getTemperatureMax() {
        return temperatureMax;
    }

    public void setTemperatureMax(double temperatureMax) {
        this.temperatureMax = temperatureMax;
    }

    public double getTemperatureMin() {
        return temperatureMin;
    }

    public void setTemperatureMin(double temperatureMin) {
        this.temperatureMin = temperatureMin;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }
}
