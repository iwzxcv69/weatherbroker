package ru.ivankov.weatherBroker.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class WeatherParametersDto {

    private double temperatureNow;
    private double temperatureFelt;
    private double temperatureMax;
    private double temperatureMin;
    private double pressure;
    private double humidity;

    @JsonCreator
    public WeatherParametersDto(
            @JsonProperty("temp") double temperatureNow,
            @JsonProperty("feels_like") double temperatureFelt,
            @JsonProperty("temp_max") double temperatureMax,
            @JsonProperty("temp_min") double temperatureMin,
            @JsonProperty("pressure") double pressure,
            @JsonProperty("humidity") double humidity) {
        this.temperatureNow = temperatureNow;
        this.temperatureFelt = temperatureFelt;
        this.temperatureMax = temperatureMax;
        this.temperatureMin = temperatureMin;
        this.pressure = pressure;
        this.humidity = humidity;
    }
}
