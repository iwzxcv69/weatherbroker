package ru.ivankov.weatherBroker.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.ivankov.weatherBroker.service.CityWeatherService;
import ru.ivankov.weatherBroker.service.WeatherService;

@Controller
@RestController
public class WeatherController {

    private final WeatherService weatherService;

    public WeatherController(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    @GetMapping("/")
    private String home(Model model, @RequestParam(required = false) CityWeatherService city) {
        if (city != null) {
            model.addAttribute("weather", weatherService.getWeatherInTheCity(city));
        }
        return "home";
    }
}
