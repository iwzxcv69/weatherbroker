package ru.ivankov.weatherBroker.error;

public class CityNotFoundException extends RuntimeException {

    public CityNotFoundException() {
        super("Город не найден.");
    }
}
