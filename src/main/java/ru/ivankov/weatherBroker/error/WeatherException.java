package ru.ivankov.weatherBroker.error;

public class WeatherException extends RuntimeException {

    public WeatherException(String message) {
        super(message);
    }
}
