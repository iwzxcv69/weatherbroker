CREATE TABLE weather
(
    id               BIGINT NOT NULL,
    city             VARCHAR(255),
    temperature_now  DOUBLE PRECISION,
    temperature_felt DOUBLE PRECISION,
    temperature_max  DOUBLE PRECISION,
    temperature_min  DOUBLE PRECISION,
    pressure         DOUBLE PRECISION,
    humidity         DOUBLE PRECISION,
    CONSTRAINT pk_weather PRIMARY KEY (id)
);